import { call, put, select, delay, takeLatest } from 'redux-saga/effects';

import * as actions from './actions';
import * as constants from './constants';
import * as selectors from './selectors';

function* handleInitializeSaga(action) {
  try {
    yield put(actions.handleInitializeActionStarted());
    yield put(actions.loadingReducer({ isLoading: true }));

    let isLoading = yield select(selectors.loadingSelector);
    console.log('isLoading on Saga Started === ', isLoading);

    yield delay(5000);
    yield put(actions.loadingReducer({ isLoading: false }));

    isLoading = yield select(selectors.loadingSelector);
    console.log('isLoading on Saga Finished === ', isLoading);
  } catch (error) {
    yield put(actions.handleInitializeActionError(error));
  }
  yield put(actions.handleInitializeActionFinished());
}

export function* appSaga() {
  yield call(handleInitializeSaga);
  yield takeLatest(constants.HANDLE_INITIALIZE_SAGA, handleInitializeSaga);
}
