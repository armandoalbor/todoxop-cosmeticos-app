import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager
} from 'react-native-fbsdk';

const PERMISSIONS = ['email'];
const QUERY_STRING =
  'id,name,first_name,middle_name,last_name,age_range,birthday,about,email,gender,languages,picture.width(200).height(200){height,width,url,cache_key,is_silhouette},address,public_key,link,short_name,name_format';

export const getUserFBData = (token, onLoginFinished) => {
  const infoRequest = new GraphRequest(
    '/me',
    {
      parameters: {
        fields: {
          string: QUERY_STRING
        },
        access_token: {
          string: token
        }
      }
    },
    onLoginFinished
  );
  new GraphRequestManager().addRequest(infoRequest).start();
};

export const logoutHandler = (onLogoutFinished) => {
  LoginManager.logOut();
  onLogoutFinished();
};

export const loginWithFacebookHandler = (onLoginFinished) => {
  LoginManager.logInWithPermissions(PERMISSIONS).then(
    (result) => {
      if (result.isCancelled) {
        console.log('Login cancelled');
      } else {
        AccessToken.getCurrentAccessToken().then((data) => {
          getUserFBData(data.accessToken.toString(), onLoginFinished);
        });
      }
    },
    (error) => {
      console.error('Login fail with error: ', error);
    }
  );
};
