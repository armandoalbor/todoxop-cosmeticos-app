import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { loginWithFacebookHandler, logoutHandler } from './handlers';
import styles from './styles';

export const FacebookButton = (props) => {
  const { isLoggedIn, onLoginFinished, onLogoutFinished } = props;

  return (
    <View style={styles.buttonContainer}>
      {isLoggedIn ? (
        <TouchableOpacity
          style={[styles.button, styles.buttonLogout]}
          onPress={() => logoutHandler(onLogoutFinished)}>
          <Text style={styles.buttonText}>Logout</Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={styles.button}
          onPress={() => loginWithFacebookHandler(onLoginFinished)}>
          <Text style={styles.buttonText}>Login With Facebook</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};
