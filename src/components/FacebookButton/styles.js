import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16
  },
  button: {
    borderRadius: 4,
    height: 48,
    width: 240,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c79ea'
  },
  buttonText: {
    color: '#ffffff',
    fontWeight: '500'
  },
  buttonLogout: {
    borderWidth: 1,
    borderColor: '#e31414',
    backgroundColor: '#e31414'
  }
});

export default styles;
