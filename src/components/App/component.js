import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image
} from 'react-native';

// import { FacebookButton } from '../FacebookButton';
import styles from './styles';

export const App = () => {
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [userData, setUserData] = React.useState({});

  const onLoginFinished = (error, result) => {
    if (error) {
      console.error(error);
    } else {
      console.log('User', result);

      setUserData(result);
      setIsLoggedIn(true);
    }
  };

  const onLogoutFinished = () => {
    setIsLoggedIn(false);
    setUserData({});
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            {userData.picture && (
              <View style={styles.imgContainer}>
                <Image
                  style={styles.img}
                  source={{ uri: userData.picture.data.url }}
                />
              </View>
            )}

            {Object.keys(userData)
              .filter(
                (i) => i !== 'location' && i !== 'age_range' && i !== 'picture'
              )
              .sort()
              .map((key, i) => {
                return (
                  <View style={styles.sectionContainer} key={`${key}-${i}`}>
                    <Text style={styles.sectionTitle}>{key}</Text>
                    <Text style={styles.sectionDescription}>
                      {userData[key]}
                    </Text>
                  </View>
                );
              })}

            {/* <FacebookButton
              isLoggedIn={isLoggedIn}
              onLoginFinished={onLoginFinished}
              onLogoutFinished={onLogoutFinished}
            /> */}
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};
