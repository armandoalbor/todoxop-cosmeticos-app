import { all, fork } from 'redux-saga/effects';

import { appSaga } from '../store/App';

export function* rootSaga() {
  yield all([
    fork(appSaga)
  ]);
}
