import React from 'react';
import { Provider } from 'react-redux';

import { AppContainer } from '../store/App';
import { store } from './configureStore';

export const AppRoot = () => {
  return (
    <Provider store={store}>
      <AppContainer />
    </Provider>
  );
};
