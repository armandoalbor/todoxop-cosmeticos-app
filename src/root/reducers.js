import { combineReducers } from 'redux';

import { appReducer, id as appId } from '../store/App';

export const rootReducer = combineReducers({
  [appId]: appReducer
});
