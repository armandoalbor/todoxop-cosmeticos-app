/**
 * @format
 */

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import { AppRoot as App } from './src/root';

AppRegistry.registerComponent(appName, () => App);
